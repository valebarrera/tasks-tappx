<?php

namespace App\Service;

use App\Api\Form\Model\ResponseDto;
use App\Model\TaskDTO;
use DateTime;
use Exception;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;


class TaskService
{
    /** @var FileService $fileService */
    private $fileService;

    /** @var string $fileName */
    private $fileName;

    /** @var SerializerInterface $serializer */
    private $serializer;

    /** @var ResponseDto $response */
    private $response;

    public function __construct(FileService $fileService, string $fileName, SerializerInterface $serializer)
    {
        $this->fileService = $fileService;
        $this->fileName = $fileName;
        $this->serializer = $serializer;
        //$this->serializer = new Serializer([new ObjectNormalizer(), new ArrayDenormalizer()],[new JsonEncoder()]);
        $this->response = new ResponseDto();
    }

    public function getAllTasks()
    {
        $data = $this->fileService->readFile($this->fileName);
        $this->response->setCode(Response::HTTP_OK);
        $this->response->setMessage('Tasks successfully recovered');
        $aux = $this->serializer->deserialize($data, TaskDTO::class . '[]', 'json');
        $this->response->setData($aux);
        return $this->response;
}

    public function getOneTask(string $id)
    {
        $all = $this->getAllTasks()->getData();
        /** @var TaskDTO $value */
        foreach ($all as $value) {
            if ($value->getId() == $id){
                $this->response->setCode(Response::HTTP_OK);
                $this->response->setMessage('Task successfully recovered');
                $this->response->setData($value);
                return $this->response;
            }
        }
        $this->response->setCode(Response::HTTP_BAD_REQUEST);
        $this->response->setMessage('Task not found');
        $this->response->setData(null);
        return $this->response;
    }

    public function registerTask(Form $form, TaskDTO $task)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            
            $all = $this->getAllTasks()->getData();
            $task->setId(uniqid());
            $task->setCreatedAt(new DateTime());
            $task->setUpdatedAt($task->getCreatedAt());
            $all[] = $task;
            $this->fileService->writeFile($this->fileName,$this->serializer->serialize($all,'json'));
            $this->response->setMessage('Task successfully registered');
            $this->response->setData($task);
            return $this->response;
        }
        $this->response->setCode(Response::HTTP_BAD_REQUEST);
        $this->response->setMessage('The task could not be registered, the data sent is not valid');
        $this->response->setData($form->getErrors()->getForm()->getErrors());
        return $this->response;
    }

    public function updateTask(Form $form, TaskDTO $task)
    {
        if ($form->isSubmitted() && $form->isValid()) {
            $task->setUpdatedAt(new DateTime());
            $all = $this->getAllTasks()->getData();
            $key_task = null;
            foreach ($all as $key => $value) {
                if ($value->getId() == $task->getId()){
                    $key_task = $key;
                }
            }
            $all[$key_task] = $task;
            $this->fileService->writeFile($this->fileName,$this->serializer->serialize($all,'json'));
            return $task;
        }
        return $form->getErrors()->getForm();
    }

    public function removeTask(string $id)
    {
        $all = $this->getAllTasks()->getData();
        $key_task = null;
        foreach ($all as $key => $value) {
            if ($value->getId() == $id){
                $key_task = $key;
            }
        }
        unset($all[$key_task]);
        $this->fileService->writeFile($this->fileName,$this->serializer->serialize($all,'json'));
        $this->response->setCode(Response::HTTP_OK);
        $this->response->setMessage('Task successfully removed');
        $this->response->setData(null);
        return $this->response;
    }
}