<?php

namespace App\Controller;

use App\Form\Type\TaskFormType;
use App\Model\TaskDTO;
use App\Service\TaskService;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractFOSRestController
{
    /** @var TaskService $taskService */
    private $taskService;
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @Rest\Get("/tasks/get-all", name="get_all_tasks")
     * @return View
     */
    public function getAllTasks()
    {
        return $this->view($this->taskService->getAllTasks(),Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/tasks/get-one/{id}", name="get_one_task")
     * @param string $id
     * @return View
     */
    public function getOneTask(string $id)
    {
        return $this->view($this->taskService->getOneTask($id),Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/tasks/register", name="register_task")
     * @param Request $request
     * @return View
     */
    public function registerTask(Request $request)
    {
        $task = new TaskDTO();
        $form = $this->createForm(TaskFormType::class, $task);
        $form->handleRequest($request);
        return $this->view($this->taskService->registerTask($form, $task), Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/tasks/update/{id}", name="update_task")
     * @param Request $request
     * @param string $id
     * @return View
     */
    public function updateTask(Request $request, string $id)
    {
        /** @var TaskDTO $task */
        $task = $this->taskService->getOneTask($id)->getData();
        $form = $this->createForm(TaskFormType::class, $task);
        $form->handleRequest($request);
        return $this->view($this->taskService->updateTask($form, $task), Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/tasks/remove/{id}", name="remove_task")
     * @param string $id
     * @return View
     */
    public function removeTask(string $id)
    {
        return $this->view($this->taskService->removeTask($id),Response::HTTP_OK);
    }
}
