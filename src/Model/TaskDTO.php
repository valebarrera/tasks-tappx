<?php

namespace App\Model;

use DateTime;
use DateTimeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TaskDTO
{
    /**
     * @var string
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @Assert\NotNull
     * @var string
     */
    private $title;

    /**
     * 
     * @var DateTimeInterface
     */
    private $createdAt;

    /**
     * 
     * @var DateTimeInterface
     */
    private $updatedAt;

    /**
     * @Assert\NotNull
     * @var bool
     */
    private $done;
    
    /**
     * Get the value of id
     *
     * @return  string
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  string  $id
     *
     * @return  self
     */ 
    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of title
     *
     * @return  string
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @param  string  $title
     *
     * @return  self
     */ 
    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of createdAt
     *
     * @return  DateTimeInterface
     */ 
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set the value of createdAt
     *
     * @param  $createdAt
     *
     * @return  self
     */ 
    public function setCreatedAt($createdAt)
    {
        if (is_string($createdAt)) {
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $createdAt);
        }
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of updatedAt
     *
     * @return  DateTimeInterface
     */ 
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set the value of updatedAt
     *
     * @param  $updatedAt
     *
     * @return  self
     */ 
    public function setUpdatedAt($updatedAt)
    {
        if (is_string($updatedAt)) {
            $createdAt = DateTime::createFromFormat('Y-m-d H:i:s', $updatedAt);
        }
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of done
     *
     * @return  bool
     */ 
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set the value of done
     *
     * @param  bool  $done
     *
     * @return  self
     */ 
    public function setDone(?bool $done)
    {
        $this->done = $done ? : false;

        return $this;
    }
}