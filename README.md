Para correr este proyecto es necesario contar con la versión php@8.1 y symfony 5.4

Instrucciones para inicializar el ambiente de trabajo:
 
- Ejecutar en el directorio raíz del proyecto [php] composer install

- Para incorporar los datos de prueba puede reemplazar el archivo data.json que se encuentra en la carpeta data del proyecto.

- Para comprobar el correcto funcionamiento del proyecto debe ejecutar los test con la siguiente instrucción: ./vendor/bin/phpunit
- Luego de que los test se ejecutaron correctamente puede dejar sirviendo el proyecto con la siguiente instrucción: symfony serve
- Para conocer cada uno de los endpoints y sus características/requerimientos visite la siguiente url con documentación:
      https://documenter.getpostman.com/view/21864591/UzR1L3Yo

